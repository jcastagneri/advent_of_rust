use std::fs;
#[macro_use(lazy_static)]
extern crate lazy_static;

pub mod week_1 {
    pub fn day_1(expenses: &Vec<i32>, num_candidates: i32) -> i32 {
        // Given a vector of integers, find which num_candidates integers sum to 2020
        // and return their multiple
        let target_sum: i32 = 2020;
        if num_candidates == 2 {
            for first_value in expenses.iter() {
                match expenses.iter().find(|&&x| x + first_value == target_sum) {
                    None => continue,
                    Some(candidate) => return candidate * first_value,
                }
            }
            panic!("No items in the vector summed to {}", target_sum);
        } else if num_candidates == 3 {
            for first_value in expenses.iter() {
                for second_value in expenses.iter() {
                    match expenses.iter().find(|&&x| x + first_value + second_value == target_sum) {
                        None => continue,
                        Some(candidate) => return candidate * first_value * second_value,
                    }
                }
            }
            panic!("No items in the vector summed to {}", target_sum);
        } else {
            panic!("Only num_candidates 2 or 3 are supported.")
        }
    }

    pub fn day_2(password_rules: Vec<(String, char, String)>) -> Vec<bool> {
        // Takes a vector of password rules (see day_2.txt for examples) and processes them
        // for validity. Consumes the vector of rules, generating a vector of validity bools in its
        // place
        let mut results: Vec<bool> = Vec::new();
        for password_rule in password_rules.iter() {
            results.push(day_2_check_password(password_rule))
        }
        return results;
    }

    fn day_2_check_password(password_rule: &(String, char, String)) -> bool {
        // Given a password rule with:
        //    counts  : \d-\d range of valid occurrences of
        //    letter  : The letter to check for counts of in
        //    password: The string to assess for counts range of letter
        //
        // If password satisfies the check, returns true.

        let (counts, letter, password) = password_rule;

        // Parse rule into min and max counts
        let min_count = &counts[..1].parse::<i32>();
        let min_count: &i32 = match min_count {
            Ok(min_count)=> min_count,
            Err(e) => {
                panic!("Counts wasn't a hyphen separated pair of integers: {:?}", e);
            }
        };
        let max_count = &counts[2..].parse::<i32>();
        let max_count: &i32 = match max_count {
            Ok(max_count)=> max_count,
            Err(e) => {
                panic!("Counts wasn't a hyphen separated pair of integers: {:?}", e);
            }
        };
        let matches: i32 = password.matches(*letter).count() as i32;

        return if &matches >= min_count && &matches <= max_count {
            true
        } else {
            false
        }
    }

    pub fn day_3(forest_map: &Vec<String>, slope_rise_modifier: usize, slope_run_modifier: usize) -> i32 {
        // Takes a vector of strings representing the forest map (see day_3.txt for examples) and
        // processes it to compute how many trees are hit if we go three to the right and one down
        // until through the map. Returns number of # chars hit.
        let mut num_trees: i32 = 0;
        let mut next_index: usize = 0;
        for row_in_map in forest_map.iter().step_by(slope_rise_modifier) {
            let char_vec = row_in_map.chars().collect::<Vec<char>>();
            if char_vec[next_index] == '#' {
                num_trees += 1;
            }
            next_index = (next_index + slope_run_modifier) % char_vec.len();
        }
        return num_trees;

    }

    pub fn day_4(passports: &Vec<Vec<String>>) -> (i32, i32) {
        use std::collections::HashSet;
        let mut present_passports: i32 = 0;
        let mut valid_passports: i32 = 0;
        let expected_keys = ["byr".to_string(), "iyr".to_string(),
            "eyr".to_string(), "hgt".to_string(), "hcl".to_string(), "ecl".to_string(),
            "pid".to_string()];
        let expected_keys: HashSet<_> = expected_keys.iter().cloned().collect();
        for passport in passports.iter() {
            // Assemble keys from the password
            let mut keys: HashSet<String> = HashSet::new();
            for key_value in passport.iter() {
                let key: &str = key_value.split(":").collect::<Vec<&str>>()[0];
                keys.insert(key.to_string());
            }
            let intersection: HashSet<_> = expected_keys.intersection(&keys).collect();
            let unwrapped_expected = expected_keys.iter().collect();
            if intersection == unwrapped_expected {
                present_passports += 1;
                valid_passports += validate_passport(&passport);

            }
        }


        return (present_passports, valid_passports);
    }

    fn validate_passport(passport: &Vec<String>) -> i32 {
        use regex::Regex;
        let mut keys_values: Vec<(String, String)> = Vec::new();
        for key_value in passport.iter() {
            let split = key_value.split(":").collect::<Vec<&str>>();
            keys_values.push((split[0].to_string(), split[1].to_string()));
        }
        let mut valid: bool = true;
        for (key, value) in keys_values.iter() {
            valid &= match key.as_str() {
                "byr" => {
                    let byr = value.parse::<i32>().expect("failed to cast");
                    let is_valid = 1920 <= byr && byr <= 2002;
                    is_valid
                },
                "iyr" => {
                    let iyr = value.parse::<i32>().expect("failed to cast");
                    let is_valid = 2010 <= iyr && iyr <= 2020;
                    is_valid
                },
                "eyr" => {
                    let eyr = value.parse::<i32>().expect("failed to cast");
                    let is_valid = 2020 <= eyr && eyr <= 2030;
                    is_valid
                },
                "hgt" => {
                    lazy_static! {
                        static ref RE: Regex = Regex::new(r"(\d\d\d|\d\d)(cm|in)").unwrap();
                    }
                    let mut is_valid = RE.is_match(value);
                    if is_valid {
                        let cap = RE.captures(value).unwrap();
                        let height = cap[1].parse::<i32>().expect("Failed to cast");
                        is_valid = match &cap[2] {
                            "cm" => {
                                150 <= height && height <= 193
                            },
                            "in" => {
                                59 <= height && height <= 76
                            },
                            _ => {
                                false
                            }
                        };
                    }
                    is_valid
                },
                "hcl" => {
                    lazy_static! {
                        static ref RE: Regex = Regex::new(r"#([a-f]|[0-9]){6}").unwrap();
                    }
                    let mut is_valid = RE.is_match(value);
                    is_valid
                },
                "ecl" => {
                    match value.as_str() {
                        "amb" => true,
                        "blu" => true,
                        "brn" => true,
                        "gry" => true,
                        "grn" => true,
                        "hzl" => true,
                        "oth" => true,
                        _ => false,
                    }
                },
                "pid" => {
                    lazy_static! {
                        static ref RE: Regex = Regex::new(r"\d{9}").unwrap();
                    }
                    let mut is_valid = RE.is_match(value);
                    is_valid
                },
                "cid" => true,
                _ => {
                    panic!("How did we get here")
                }
            }

        }
        let mut valid_as_int: i32 = 0;  // Sinfully cast back to i32 so we can count easier.
        if valid {valid_as_int = 1;}

        return valid_as_int;
    }

    pub fn day_5(encoded_seat_numbers: &Vec<(String, String)>) -> (i32, i32) {
        let mut max_seat_number = 0;
        let mut seat_numbers = Vec::new();
        for (encoded_row, encoded_seat) in encoded_seat_numbers.iter() {
            let mut decoded_row = 0;
            let mut decoded_seat = 0;
            for (i, character) in encoded_row.chars().enumerate() {
                decoded_row = match character {
                    'B' => {
                        if i < 6 {
                            (decoded_row + 1) << 1
                        } else {
                            decoded_row + 1
                        }
                    },
                    'F' => {
                        if i < 6 {
                            decoded_row << 1
                        } else {
                            decoded_row
                        }
                    },
                    _ => decoded_row
                };

            }
            for (i, character) in encoded_seat.chars().enumerate() {
                decoded_seat = match character {
                    'R' => {
                        if i < 2 {
                            (decoded_seat + 1) << 1
                        } else {
                            decoded_seat + 1
                        }
                    },
                    'L' => {
                        if i < 2 {
                            decoded_seat << 1
                        } else {
                            decoded_seat
                        }
                    },
                    _ => decoded_seat
                };

            }
            let seat_number = decoded_row * 8 + decoded_seat;
            if seat_number > max_seat_number {
                max_seat_number = seat_number;
            }
            seat_numbers.push(seat_number);
        }
        seat_numbers.sort_by(|a, b| b.cmp(a));

        // Find missing seat from the list of seat numbers
        let mut seat_diff = 0;
        let mut my_seat = 0;
        for (i, seat_number) in seat_numbers.iter().enumerate() {
            seat_diff = seat_number - seat_numbers[i+1];
            if seat_diff != 1 {
                my_seat = seat_number - 1;
                break;
            }
        }
        return (seat_numbers[0], my_seat);
    }

    #[cfg(test)]
    mod test_week_1 {
        use super::*;

        #[test]
        fn day_2_test_check_password() {
            let password_rule = (String::from("1-3"), 'a', String::from("abcda"));
            assert!(day_2_check_password(&password_rule));

            let password_rule = (String::from("1-3"), 'a', String::from("aacaa"));
            assert!(!day_2_check_password(&password_rule));
        }

        #[test]
        fn day_2_test_check_passwords() {
            let password_rules = vec![
                (String::from("1-3"), 'a', String::from("abcde")),
                (String::from("3-4"), 'a', String::from("aadea")),
                (String::from("1-3"), 'b', String::from("cedfg")),
                (String::from("2-9"), 'c', String::from("ccccccccc"))
            ];
            let matches = day_2(password_rules);
            assert_eq!(matches, vec![true, true, false, true])
        }

        #[test]
        fn day_1_test_contrived_with_2_inputs() {
            let expenses: Vec<i32> = vec![1721, 979, 366, 299, 675, 1456];
            assert_eq!(day_1(&expenses, 2), 514579);
        }

        #[test]
        fn day_1_test_contrived_with_3_inputs() {
            let expenses: Vec<i32> = vec![1721, 979, 366, 299, 675, 1456];
            assert_eq!(day_1(&expenses, 3), 241861950);
        }

        #[test]
        #[should_panic]
        fn day_1_test_panics() {
            let expenses: Vec<i32> = vec![979, 366, 299, 675, 1456];
            day_1(&expenses, 2);
        }
    }
}

pub mod load_data {
    use super::*;
    use std::io::{BufRead, BufReader};
    use std::fs::File;

    pub fn load_file(dirname: &String, filename: &str) -> Vec<i32> {
        let mut full_path = String::from(dirname);
        full_path.push_str("/");
        full_path.push_str(filename);

        let contents = fs::read_to_string(full_path)
            .expect("Cannot read file.");
        let vec: Vec<&str> = contents.split("\n").collect();
        let vec: Vec<i32> = vec.iter().flat_map(|x| x.parse::<i32>()).collect();
        return vec
    }

    pub fn load_day_2(dirname: &String, filename: &str) -> Vec<(String, char, String)> {
        let mut full_path = String::from(dirname);
        full_path.push_str("/");
        full_path.push_str(filename);

        let reader = BufReader::new(File::open(full_path).expect("Cannot open file"));

        let mut data: Vec<(String, char, String)> = Vec::new();
        for line in reader.lines() {
            let unwrapped_line = line.unwrap();
            let mut first_word: String = String::new();
            let mut second_word: String = String::new();
            let mut third_word: String = String::new();
            for (i, word) in unwrapped_line.split_whitespace().enumerate() {
                if i == 0 {
                    first_word = String::from(word);
                } else if i == 1 {
                    second_word = String::from(&word[..1]);
                } else if i == 2 {
                    third_word = String::from(word);
                } else {
                    //data.push((first_word, second_word))
                }
            }
            data.push((first_word, second_word.chars().collect::<Vec<char>>()[0], third_word))
        }
        return data;
    }

    pub fn load_day_3(dirname: &String, filename: &str) -> Vec<String> {
        let mut full_path = String::from(dirname);
        full_path.push_str("/");
        full_path.push_str(filename);

        let reader = BufReader::new(File::open(full_path).expect("Cannot open file"));
        let mut data: Vec<String> = Vec::new();
        for line in reader.lines() {
           data.push(line.unwrap());
        }
        return data;
    }

    pub fn load_day_4(dirname: &String, filename: &str) -> Vec<Vec<String>>{
        let mut full_path = String::from(dirname);
        full_path.push_str("/");
        full_path.push_str(filename);

        let reader = BufReader::new(File::open(full_path).expect("Cannot open file"));
        let mut data: Vec<Vec<String>> = Vec::new();
        let mut passport: Vec<String> = Vec::new();
        for line in reader.lines() {
            let unwrapped_line = line.unwrap();
            match unwrapped_line.as_str() {
                "" => {
                    passport.sort();  // Alphabetize for easy lookup later
                    data.push(passport);
                    passport = Vec::new();
                    continue;
                },
                _ => {
                    for word in unwrapped_line.split_whitespace() {
                        passport.push(String::from(word));
                    }
                }
            }
        }
        return data;
    }

    pub fn load_day_5(dirname: &String, filename: &str) -> Vec<(String, String)>{
        let mut full_path = String::from(dirname);
        full_path.push_str("/");
        full_path.push_str(filename);

        let reader = BufReader::new(File::open(full_path).expect("Cannot open file"));
        let mut data: Vec<(String, String)> = Vec::new();
        for line in reader.lines() {
            let unwrapped_line = line.unwrap();
            data.push((unwrapped_line[..7].to_string(), unwrapped_line[7..].to_string()));
        }
        return data;
    }
}