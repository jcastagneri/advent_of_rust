use std::env;
use adventlib::week_1;
use adventlib::load_data;

fn main() {
    // CLI that calls lib functions for each day's test
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        panic!("Must only provide one argument, which should by the path to a data directory of \
        puzzle inputs.")
    }
    let filename = &args[1];

    let day_1_data = load_data::load_file(&filename, "day_1.txt");
    println!("Day 1 part 1: {}", week_1::day_1(&day_1_data, 2));
    println!("Day 1 part 2: {}", week_1::day_1(&day_1_data, 3));

    let day_2_data = load_data::load_day_2(&filename, "day_2.txt");
    let matches = week_1::day_2(day_2_data);
    let count: i32 = matches.iter().filter(|&x| *x).count() as i32;
    println!("Day 2 part 1: {} passwords matched.", count);

    let day_3_data = load_data::load_day_3(&filename, "day_3.txt");
    let num_trees_slope_1 = week_1::day_3(&day_3_data,1, 1);
    let num_trees_slope_3 = week_1::day_3(&day_3_data,1, 3);
    let num_trees_slope_5 = week_1::day_3(&day_3_data, 1, 5);
    let num_trees_slope_7 = week_1::day_3(&day_3_data, 1, 7);
    let num_trees_slope_1_skip = week_1::day_3(&day_3_data, 2, 1);

    let cum_trees = num_trees_slope_1 * num_trees_slope_3 * num_trees_slope_5 * num_trees_slope_7 * num_trees_slope_1_skip;
    //let num_trees_slope_1 = week_1::day_3(&day_3_data, 1);
    println!("Day 3 part 1: {} trees hit.", num_trees_slope_3);
    println!("Day 3 part 2: {} multiplied trees hit.", cum_trees);


    let day_4_data = load_data::load_day_4(&filename, "day_4.txt");
    let (present_passports, valid_passports) = week_1::day_4(&day_4_data);
    println!("Day 4 part 1: {}", present_passports);
    println!("Day 4 part 2: {}", valid_passports);


    let day_5_data = load_data::load_day_5(&filename, "day_5.txt");
    let (max_seat_id, my_seat): (i32, i32) = week_1::day_5(&day_5_data);
    println!("Day 5 part 1: Max seat ID {}", max_seat_id);
    println!("Day 5 part 2: My seat ID {}", my_seat);


}